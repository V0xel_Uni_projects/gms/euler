#include <vector>
#include <cassert>

using graph = std::vector< std::vector<int> >;

void eulerDFS(graph& graph, std::vector<int>& s, int start = 0)
{
	//Checking neighbours
	for (size_t i = 0; i < graph.size(); ++i)
	{
		while (graph[start][i])
		{
			--graph[start][i];
			--graph[i][start];
			eulerDFS(graph, s, i);
		}
	}
	s.push_back(start);
}

int main()
{
	FILE* input = nullptr;
	const errno_t errInput = fopen_s(&input, "./tests/input", "r");
	FILE* output = nullptr;
	const errno_t errOutput = fopen_s(&output, "./tests/main_out", "w");
	assert(errInput == 0 && "Failed to open file");
	assert(errOutput == 0 && "Failed to create file");

	int tests = 0, verts = 0, edges = 0;
	fscanf_s(input, "%d%*c", &tests);
	//All tests loop
	for (int t = 0; t < tests; ++t)
	{
		fscanf_s(input, "n=%d,m=%d%*c", &verts, &edges);
		graph graph(verts, std::vector<int>(verts, 0));
		std::vector<int>stack;

		int fromV, toV;
		for (int i = 0; i < edges; ++i)
		{
			fscanf_s(input, "{%d,%d} ", &fromV, &toV);
			graph[fromV][toV] = 1;
			graph[toV][fromV] = 1;
		}

		eulerDFS(graph, stack);
		for (auto& el : stack)
		{
			fprintf_s(output, "%d ", el);
		}
		fprintf_s(output, "\n");
	}

	fclose(input);
	fclose(output);
	return 0;
}
